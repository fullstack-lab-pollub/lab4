Aby utworzyć przestrzeń nazw o nazwie lab4 należy użyć następującego polecnia:
![](/lab4_1.png)
Potem należało uruchomić 5 Podów (cpu=1000m i memory=1Gi). W tym celu skorzystałem z przydziału (ang. Quota) dla przestrzeni nazw lab4:
![](/lab4_2.png)
Aby potwierdzić, że ograniczenia zostały poprawnie zastosowane, użyłem następującego polecenia:
![](/lab4_3.png)

Następnie uruchomiłem Deployment o nazwie restrictednginx w przestrzeni nazw lab4 z 3 Pod-ami gdzie każdy Pod początkowo żąda: 64 MiB RAM oraz 125m CPU, a górny limit zasobów to 256 MiB RAM oraz 250m CPU:
![](/Lab4_4.png)
Powyżej wymienione parametry ustawiłem za pomoca nstępującego polecnenia:
![](/Lab4_5.png)

Później sprawdziłem czy Deployment został poprawnie utworzony:
![](/Lab4_6.png)
Także sprawdziłem stan utworzonych Podów w przestrzeni nazw lab4:
![](/Lab4_7.png)
Sprawdziłem również opis pierwszego Poda, którego nazwa została wyświetlona po użyciu powyższego polecenia:
![](/Lab4_8.png)
Ostatecznie wygenerowałem pliki YAML działających już przestrzeni nazw, zasobów ograniczających oraz Deployment za pomocą, poniższych poleceń:
![](/Lab4_9.png)
